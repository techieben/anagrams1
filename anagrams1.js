//click handler
const button = document.getElementById("findButton");
button.onclick = function () {
  let typedText = document.getElementById("input").value;
  printArray(getAnagramsOf(typedText), "output");
};

//find anagrams of a string and return as an array of strings
function getAnagramsOf(stringParam) {
  let newArray = [];
  let alphaString = alphabetize(stringParam);
  let index = 0;
  while (wordsAlphabetized.includes(alphaString)) {
    index = wordsAlphabetized.indexOf(alphaString);
    newArray.push(words[index]);
    wordsAlphabetized[index] = 0;
  }
  return newArray;
}

//alphabetize a string
function alphabetize(stringParam) {
  console.log("running alhpabetize");
  console.log(stringParam);
  return stringParam.toLowerCase().split("").sort().join("").trim();
}

//print an array with line breaks
function printArray(arrayParam, outputDiv) {
  for (let item in arrayParam) {
    const span = document.createElement("span");
    const textContent = document.createTextNode(arrayParam[item]);
    span.appendChild(textContent);
    document.getElementById("" + outputDiv + "").appendChild(span);
    document
      .getElementById("" + outputDiv + "")
      .appendChild(document.createElement("br"));
  }
}

//alphabetize each string in an array of strings and return a new array
//used to generate words-alphabetized.js along with alternate version of printArray that includes quotes and commas
function alphabetizeWords(arrayParam) {
  let newArray = [];
  let i = 0;
  while (i < arrayParam.length) {
    newArray.push(alphabetize(arrayParam[i]));
    i++;
  }
  return newArray;
}
